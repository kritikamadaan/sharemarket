import React from 'react';
import Progress from 'react-progressbar';

function Portfolio(props) {
  const {
    price,
    quantity,
    investedAmount,
    netInvested,
  } = props.data;

  const marketValue = price * quantity;
  const percentagePortfolio = (investedAmount/netInvested) * 100;

  return (
    <div className="card card-quantity">
      <div className="container" style={{ height: "50%" }}>
        <section className='col'>
          <span style={{ fontWeight: 700 }}>
          Market Value
          </span>
          <span>
          % of portfolio value
          </span>
        </section>
        <section className='col'>
          <span style={{ fontWeight: 700 }}>
          ${marketValue}
          </span>
          <span style={{ fontWeight: 700 }}>
          {percentagePortfolio.toFixed(2)}%
          </span>
        </section>
      </div>
      <section style={{padding: "12px", height: "50%" }}>
        <Progress completed={percentagePortfolio} />
      </section>
    </div>
  );
}

export default Portfolio;
