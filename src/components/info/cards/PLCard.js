import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons'
import Progress from 'react-progressbar';

function PL(props) {
  const {
    price,
    quantity,
    investedAmount,
  } = props.data;

  const marketValue = price * quantity;
  const pl = marketValue - investedAmount;
  const percentageReturn = (pl * 100)/marketValue;

  const Caret = (props) => props.pl >= 0 ? (
    <FontAwesomeIcon style={{width: '25px', paddingRight: '10px', color: 'green'}} icon={faCaretUp} />
  ): (<FontAwesomeIcon style={{width: '25px', paddingRight: '10px', color: 'red'}} icon={faCaretDown} />)

  return (
    <div className="card card-quantity">
      <div className="container" style={{ height: "50%" }}>
        <section className='col'>
          <span style={{ fontWeight: 700 }}>
          Unrealized P/L
          </span>
          <span>
          % Return
          </span>
        </section>
        <section className='col'>
          <span style={{ fontWeight: 700 }}>
          {pl.toFixed(2)}
          </span>
          <span style={{ fontWeight: 700 }}>
          <Caret pl={pl}/> {Math.round(percentageReturn.toFixed(2))}%
          </span>
        </section>
      </div>
      <section className='progress-container' style={{ height: "50%" }}>
        <Progress completed={(-1 * percentageReturn > 0? -1 * percentageReturn: 0)} color='red' style={{ transform: "rotate(180deg)", width: "50%"}} />
        <Progress completed={(percentageReturn > 0? percentageReturn: 0)} style={{width: "50%"}} />
      </section>
    </div>
  );
}

export default PL;
