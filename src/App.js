import React, { useState } from 'react';
import { connect } from 'react-redux';
import { getStocks } from './redux/selectors';

import Info from './components/info';
import Nav from './components/nav';
import SlideForm from './components/slideForm';
import StockForm from './components/stockForm';
import Chart from './components/info/cards/Chart';

function App(props) {
  const [ formVisible, setVisible ] = useState(false);
  const showForm = () => setVisible(true);
  const hideForm = () => setVisible(false);

  const { stocks } = props;
  console.log('the stock is',stocks);
  return (
    <div className="App">
      <Nav showMenu={showForm}/>
      <div className="row">
      <div className="column1">
     {
      formVisible ?
        <SlideForm 
          closeMenu={hideForm}
          style={{transform: "none", transition: "transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms"}}
        >
          <StockForm/>
        </SlideForm>
      :
        false
    }
    <div className="global-container">
      {
        stocks.map(data => (<Info data={data}/>))
      }
     </div>
     <div className="col2">
     <Chart/>
     </div>
     </div>
     
     </div>
    </div>
  );
}

const mapStateToProps = state => ({ stocks: getStocks(state) });

export default connect(mapStateToProps)(App);
