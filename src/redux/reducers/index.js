import { MODIFY_CONTENT } from "../actionTypes";

const initialState = {
  stocks: [
    {
      ticker: 'AADR',
      price: 50.3,
      index: 'S&P 500 Index',
      quantity: 430,
      investedAmount: 17952.07,
    },
    {
      ticker: 'MFEM',
      price: 23.2,
      index: 'S&P 500 Index',
      quantity: 210,
      investedAmount: 4725.84,
    },
    {
      ticker: 'JPEM',
      price: 52.5,
      index: 'S&P 500 Index',
      quantity: 328,
      investedAmount: 18597.6,
    },
    {
      ticker: 'KEMQ',
      price: 20.4,
      index: 'S&P 500 Index',
      quantity: 801,
      investedAmount: 17811.04,
    },
    {
      ticker: 'KLDW',
      price: 32.9,
      index: 'S&P 500 Index',
      quantity: 523,
      investedAmount: 13765.36,
    },
    {
      ticker: 'KOIN',
      price: 25.4,
      index: 'S&P 500 Index',
      quantity: 335,
      investedAmount: 8509,
    },
  ]
};

export default function(state = initialState, action) {
  switch (action.type) {
    case MODIFY_CONTENT: {
      const toModify = action.payload;
      const modifyTicker = (stock) => {
        if (stock.ticker !== toModify.ticker) {
          return stock;
        }

        return { ...stock, ...toModify };
      }
      
      return {
        ...state,
        stocks: state.stocks.map(modifyTicker),
      }
    }
    default:
      return state;
  }
}
